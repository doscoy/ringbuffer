# ringbuffer

リングバッファ

## 使い方

```cpp
// int型128個分のバッファを静的に確保
buff::ring_array<int, 128> arr;

// int型128個分のバッファを動的に確保
buff::ring_vector<int> vec{128};

// 値の追加
arr.push(0);
arr.push(1);
arr.push(2);

// 値の取り出し
auto size{arr.size()};
for (decltype(size) i = 0; i < size; ++i) {
  std::cout << arr.front() << ' ';
  arr.pop();
}
std::cout << std::endl;

// イテレータの使用
constexpr int list[]{0, 1, 2, 3, 4};
std::copy(std::begin(list), std::end(list), std::back_inserter(arr));

for (auto i: arr) std::cout << i << ' ';
std::cout << std::endl;

std::for_each(arr.rbegin(), arr.rend(), [](int i) { std::cout << i << ' '; });
std::cout << std::endl;

// 削除
arr.clear();
```