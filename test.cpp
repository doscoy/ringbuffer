#include <gtest/gtest.h>

#include <ringbuffer.hpp>

TEST(test, PushPop) {
  const auto test{[](auto* ring) {
    ASSERT_EQ(ring->raw_size(), 4);
    //  w     b
    // | | | | |
    //  f
    ASSERT_TRUE(ring->empty());

    ring->push('0');
    // >b>w   ->
    // |0| | | |
    //  f
    ASSERT_FALSE(ring->empty());
    ASSERT_EQ(ring->size(), 1);
    ASSERT_EQ(ring->back(), '0');
    ASSERT_EQ(ring->front(), '0');

    ring->pop();
    //  b w
    // |0| | | |
    //  ->f
    ASSERT_TRUE(ring->empty());
  }};
  {
    SCOPED_TRACE("array");
    buff::ring_array<char, 4> arr{'\0'};
    test(&arr);
  }
  {
    SCOPED_TRACE("vector");
    buff::ring_vector<char> vec(4, '\0');
    test(&vec);
  }
}

TEST(test, CopyMove) {
  const auto test{[](auto* ring) {
    ring->push('0');
    ring->push('1');
    ring->push('2');

    const auto sub_test{[](auto* ring){
      auto tmp{*ring};
      ASSERT_EQ(ring->back(), tmp.back());
      ASSERT_EQ(ring->size(), tmp.size());
      auto size{ring->size()};
      for (decltype(size) i = 0; i < size; ++i) {
        ASSERT_EQ(ring->front(), tmp.front());
        ring->pop();
        tmp.pop();
      }
    }};
    {
      SCOPED_TRACE("copy");
      sub_test(ring);
    }
    {
      SCOPED_TRACE("move");
      auto mv{std::move(*ring)};
      sub_test(&mv);
    }

  }};
  {
    SCOPED_TRACE("array");
    buff::ring_array<char, 4> arr{'\0'};
    test(&arr);
  }
  {
    SCOPED_TRACE("vector");
    buff::ring_vector<char> vec(4, '\0');
    test(&vec);
  }
}

TEST(test, OverFlow) {
  const auto test{[](auto* ring) {
    ASSERT_EQ(ring->raw_size(), 3);
    //  w   b
    // | | | |
    //  f
    ASSERT_TRUE(ring->empty());

    ring->push('0');
    // >b>w ->
    // |0| | |
    //  f
    ASSERT_EQ(ring->size(), 1);
    ASSERT_EQ(ring->back(), '0');
    ASSERT_EQ(ring->front(), '0');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '\0');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('1');
    //  ->b>w
    // |0|1| |
    //  f
    ASSERT_EQ(ring->size(), 2);
    ASSERT_EQ(ring->back(), '1');
    ASSERT_EQ(ring->front(), '0');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '\0');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('2');
    // >w ->b>
    // |0|1|2|
    //  ->f
    ASSERT_EQ(ring->size(), 2);
    ASSERT_EQ(ring->back(), '2');
    ASSERT_EQ(ring->front(), '1');
    ASSERT_EQ(ring->drop_count(), 1);
    ASSERT_EQ(ring->drop_value(), '0');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('3');
    // >b>w ->
    // |3|1|2|
    //    ->f
    ASSERT_EQ(ring->size(), 2);
    ASSERT_EQ(ring->back(), '3');
    ASSERT_EQ(ring->front(), '2');
    ASSERT_EQ(ring->drop_count(), 1);
    ASSERT_EQ(ring->drop_value(), '1');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('4');
    //  ->b>w   
    // |3|4|2|
    // >f   ->
    ASSERT_EQ(ring->size(), 2);
    ASSERT_EQ(ring->back(), '4');
    ASSERT_EQ(ring->front(), '3');
    ASSERT_EQ(ring->drop_count(), 1);
    ASSERT_EQ(ring->drop_value(), '2');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('5');
    // >w ->b>
    // |3|4|5|
    //  ->f
    ASSERT_EQ(ring->size(), 2);
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '4');
    ASSERT_EQ(ring->drop_count(), 1);
    ASSERT_EQ(ring->drop_value(), '3');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->pop();
    //  w   b
    // |3|4|5|
    //    ->f
    ASSERT_EQ(ring->size(), 1);
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '5');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '3');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->pop();
    //  w   b
    // |3|4|5|
    // >f   ->
    ASSERT_TRUE(ring->empty());
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '3');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '3');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->pop();
    //  w   b
    // |3|4|5|
    //  f
    ASSERT_TRUE(ring->empty());
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '3');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '3');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->clear();
    //  w   b
    // |3|4|5|
    //  f
    ASSERT_TRUE(ring->empty());
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '3');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '3');
    ASSERT_EQ(ring->drop_count(), 0);
  }};
  {
    SCOPED_TRACE("array");
    buff::ring_array<char, 3> arr{'\0'};
    test(&arr);
  }
  {
    SCOPED_TRACE("vector");
    buff::ring_vector<char> vec(3, '\0');
    test(&vec);
  }
}

TEST(test, OverFlowPow2) {
  const auto test{[](auto* ring) {
    ASSERT_EQ(ring->raw_size(), 4);
    //  w     b
    // | | | | |
    //  f
    ASSERT_TRUE(ring->empty());

    ring->push('0');
    // >b>w   ->
    // |0| | | |
    //  f
    ASSERT_EQ(ring->size(), 1);
    ASSERT_EQ(ring->back(), '0');
    ASSERT_EQ(ring->front(), '0');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '\0');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('1');
    //  ->b>w
    // |0|1| | |
    //  f
    ASSERT_EQ(ring->size(), 2);
    ASSERT_EQ(ring->back(), '1');
    ASSERT_EQ(ring->front(), '0');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '\0');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('2');
    //    ->b>w
    // |0|1|2| |
    //  f
    ASSERT_EQ(ring->size(), 3);
    ASSERT_EQ(ring->back(), '2');
    ASSERT_EQ(ring->front(), '0');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '\0');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('3');
    // >w   ->b>
    // |0|1|2|3|
    //  ->f
    ASSERT_EQ(ring->size(), 3);
    ASSERT_EQ(ring->back(), '3');
    ASSERT_EQ(ring->front(), '1');
    ASSERT_EQ(ring->drop_count(), 1);
    ASSERT_EQ(ring->drop_value(), '0');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('4');
    // >b>w   ->
    // |4|1|2|3|
    //    ->f
    ASSERT_EQ(ring->size(), 3);
    ASSERT_EQ(ring->back(), '4');
    ASSERT_EQ(ring->front(), '2');
    ASSERT_EQ(ring->drop_count(), 1);
    ASSERT_EQ(ring->drop_value(), '1');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->push('5');
    //  ->b>w
    // |4|5|2|3|
    //      ->f
    ASSERT_EQ(ring->size(), 3);
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '3');
    ASSERT_EQ(ring->drop_count(), 1);
    ASSERT_EQ(ring->drop_value(), '2');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->pop();
    //    b w
    // |4|5|2|3|
    // >f     ->
    ASSERT_EQ(ring->size(), 2);
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '4');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '2');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->pop();
    //    b w
    // |4|5|2|3|
    //  ->f
    ASSERT_EQ(ring->size(), 1);
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '5');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '2');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->pop();
    //    b w
    // |4|5|2|3|
    //    ->f
    ASSERT_TRUE(ring->empty());
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '2');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '2');
    ASSERT_EQ(ring->drop_count(), 0);
    

    ring->pop();
    //    b w
    // |4|5|2|3|
    //      f
    ASSERT_TRUE(ring->empty());
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '2');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '2');
    ASSERT_EQ(ring->drop_count(), 0);

    ring->clear();
    //  w     b
    // |4|5|2|3|
    //  f
    ASSERT_TRUE(ring->empty());
    ASSERT_EQ(ring->back(), '3');
    ASSERT_EQ(ring->front(), '4');
    ASSERT_EQ(ring->drop_count(), 0);
    ASSERT_EQ(ring->drop_value(), '4');
    ASSERT_EQ(ring->drop_count(), 0);
  }};
  {
    SCOPED_TRACE("array");
    buff::ring_array<char, 4> arr{'\0'};
    test(&arr);
  }
  {
    SCOPED_TRACE("vector");
    buff::ring_vector<char> vec(4, '\0');
    test(&vec);
  }
}

TEST(test, Ref) {
  const auto test{[](auto* ring) {
    ASSERT_EQ(ring->raw_size(), 4);
    //  w     b
    // | | | | |
    //  f
    ASSERT_TRUE(ring->empty());

    ring->push('0');
    // >b>w   ->
    // |0| | | |
    //  f
    ASSERT_EQ(ring->size(), 1);
    ASSERT_EQ(ring->back(), '0');
    ASSERT_EQ(ring->front(), '0');

    ring->inc();
    ring->back() = '1';
    //  ->b>w
    // |0|1| | |
    //  f
    ASSERT_EQ(ring->size(), 2);
    ASSERT_EQ(ring->back(), '1');
    ASSERT_EQ(ring->front(), '0');

    ring->pop();
    //    b w
    // |0|1| | |
    //  ->f
    ASSERT_EQ(ring->size(), 1);
    ASSERT_EQ(ring->back(), '1');
    ASSERT_EQ(ring->front(), '1');

    ring->back() = '2';
    //    b w
    // |0|2| | |
    //    f
    ASSERT_EQ(ring->size(), 1);
    ASSERT_EQ(ring->back(), '2');
    ASSERT_EQ(ring->front(), '2');
  }};
  {
    SCOPED_TRACE("array");
    buff::ring_array<char, 4> arr{'\0'};
    test(&arr);
  }
  {
    SCOPED_TRACE("vector");
    buff::ring_vector<char> vec(4, '\0');
    test(&vec);
  }
}

TEST(test, Bydirect) {
  const auto test{[](auto* ring) {
    ASSERT_EQ(ring->raw_size(), 8);
    ASSERT_TRUE(ring->empty());
    {
      char cnt{'0'};
      while (not ring->full()) {
        ring->push(cnt);
        ++cnt;
      }
      ASSERT_EQ(ring->size(), ring->max_size());
      ASSERT_EQ(ring->back(), '6');
      ASSERT_EQ(ring->front(), '0');
    }
    auto a{++(ring->begin())};
    auto b{a};
    {
      auto aa{a};
      ASSERT_EQ(std::addressof(--aa), std::addressof(aa));
    }
    {
      auto aa{a};
      auto bb{b};
      ASSERT_EQ(aa--, bb);
    }
    {
      auto aa{a};
      auto bb{b};
      aa--;
      --bb;
      ASSERT_EQ(aa, bb);
    }
    {
      auto aa{a};
      auto bb{b};
      ASSERT_EQ(++(--aa), bb);
    }
    {
      auto aa{a};
      auto bb{b};
      ASSERT_EQ(--(++aa), bb);
    }
  }};
  {
    SCOPED_TRACE("array");
    buff::ring_array<char, 8> arr{'\0'};
    test(&arr);
  }
  {
    SCOPED_TRACE("vector");
    buff::ring_vector<char> vec(8, '\0');
    test(&vec);
  }
}

TEST(test, Rundom) {
  const auto test{[](auto* ring) {
    ASSERT_EQ(ring->raw_size(), 8);
    ASSERT_TRUE(ring->empty());
    {
      char cnt{'0'};
      while (ring->size() != ring->max_size()) {
        ring->push(cnt);
        ++cnt;
      }
      ASSERT_EQ(ring->size(), ring->max_size());
      ASSERT_EQ(ring->back(), '6');
      ASSERT_EQ(ring->front(), '0');
    }

    auto a{ring->begin()};
    using I = decltype(a);
    using D = typename I::difference_type;
    for (D n = 0; n < 10; ++n) {
      auto b{a};
      for (D i = 0; i < n; ++i) ++b;
      {
        auto aa{a};
        ASSERT_EQ(aa += n, b);
      }
      {
        auto aa{a};
        ASSERT_EQ(std::addressof(aa += n), std::addressof(aa));
      }
      {
        auto aa{a};
        ASSERT_EQ(aa += n, a + n);
      }
      {
        D x{3};
        D y{6};
        ASSERT_EQ((a + D(x + y)), ((a + x) + y));
      }
      { ASSERT_EQ(a + 0, a); }
      {
        ASSERT_EQ(a + n, [](I c) { return ++c; }(a + D(n - 1)));
      }
      {
        auto bb{b};
        ASSERT_EQ(bb += D(-n), a);
      }
      {
        auto bb{b};
        ASSERT_EQ(bb -= n, a);
      }
      {
        auto bb{b};
        ASSERT_EQ(std::addressof(bb -= n), std::addressof(bb));
      }
      {
        auto bb{b};
        ASSERT_EQ(bb - n, bb -= n);
      }
      { ASSERT_TRUE(a <= b) << "n=" << n; }
    }
  }};
  {
    SCOPED_TRACE("array");
    buff::ring_array<char, 8> arr{'\0'};
    test(&arr);
  }
  {
    SCOPED_TRACE("vector");
    buff::ring_vector<char> vec(8, '\0');
    test(&vec);
  }
}

TEST(test, Algorithm) {
  const auto test{[](auto* ring) {
    ASSERT_EQ(ring->raw_size(), 4);
    ASSERT_TRUE(ring->empty());

    constexpr char kList[]{'0', '1', '2', '3', '4', '5'};
    std::copy(std::begin(kList), std::end(kList), std::back_inserter(*ring));
    //    b w
    // |4|5|2|3|
    //        f
    ASSERT_EQ(ring->back(), '5');
    ASSERT_EQ(ring->front(), '3');
    ASSERT_EQ(ring->drop_count(), 3);

    constexpr char kCheck[]{'3', '4', '5'};
    {
      auto check_ptr{std::begin(kCheck)};
      std::for_each(ring->begin(), ring->end(), [&check_ptr](auto c) {
        ASSERT_EQ(c, *check_ptr);
        ++check_ptr;
      });
    }
    {
      auto check_ptr{std::rbegin(kCheck)};
      std::for_each(ring->rbegin(), ring->rend(), [&check_ptr](auto c) {
        ASSERT_EQ(c, *check_ptr);
        ++check_ptr;
      });
    }
  }};
  {
    SCOPED_TRACE("array");
    buff::ring_array<char, 4> arr{'\0'};
    test(&arr);
  }
  {
    SCOPED_TRACE("vector");
    buff::ring_vector<char> vec(4, '\0');
    test(&vec);
  }
}