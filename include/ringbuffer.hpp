#pragma once

#include <algorithm>
#include <cassert>
#include <iterator>
#include <type_traits>

namespace buff {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// declaration
//
//

namespace detail {

template <class RingContainer>
class ring_iterator_base {
  using self = ring_iterator_base;

 public:
  using value_type = typename RingContainer::value_type;
  using pointer = value_type*;
  using reference = value_type&;
  using const_reference = const value_type&;
  using size_type = typename RingContainer::size_type;
  using difference_type = std::make_signed_t<size_type>;
  using iterator_category = std::random_access_iterator_tag;

  ring_iterator_base(pointer buffer, size_type size, size_type index) noexcept;

  ring_iterator_base(const self&) noexcept = default;
  ring_iterator_base(self&&) noexcept = default;

  self& operator=(const self&) noexcept = default;
  self& operator=(self&&) noexcept = default;

  inline auto operator==(const self& rhs) const noexcept -> bool;
  inline auto operator!=(const self& rhs) const noexcept -> bool;
  inline auto operator<(const self& rhs) const noexcept -> bool;
  inline auto operator<=(const self& rhs) const noexcept -> bool;
  inline auto operator>(const self& rhs) const noexcept -> bool;
  inline auto operator>=(const self& rhs) const noexcept -> bool;

  inline auto operator+=(difference_type rhs) noexcept -> self&;
  inline auto operator+(difference_type rhs) noexcept -> self;
  inline auto operator-=(difference_type rhs) noexcept -> self&;
  inline auto operator-(difference_type rhs) noexcept -> self;
  inline auto operator-(const self& rhs) noexcept -> difference_type;

  inline auto operator++() noexcept -> self&;
  inline auto operator++(int) noexcept -> self;
  inline auto operator--() noexcept -> self&;
  inline auto operator--(int) noexcept -> self;

  inline auto operator->() noexcept -> reference;
  inline auto operator->() const noexcept -> const_reference;

  inline auto operator*() noexcept -> reference;
  inline auto operator*() const noexcept -> const_reference;

 private:
  pointer buffer_;
  const size_type size_;
  size_type index_;
  difference_type diff_;
};

template <class Type, class Detail>
class ring_base {
 public:
  using value_type = Type;
  using reference = value_type&;
  using const_reference = const value_type&;
  using size_type = unsigned;

  inline auto size() const noexcept -> size_type;

  inline auto max_size() const noexcept -> size_type;
  inline auto empty() const noexcept -> bool;
  inline auto full() const noexcept -> bool;

  inline auto front() noexcept -> reference;
  inline auto front() const noexcept -> const_reference;
  inline auto back() noexcept -> reference;
  inline auto back() const noexcept -> const_reference;
  inline auto drop_count() const noexcept -> size_type;
  inline auto drop_value() noexcept -> reference;
  inline auto drop_value() const noexcept -> const_reference;

  inline void inc() noexcept;
  inline void push(const_reference x) noexcept;
  inline void push_back(const_reference x) noexcept;
  inline void pop() noexcept;
  inline void pop_n(size_type n) noexcept;
  inline void clear() noexcept;

 protected:
  ring_base() noexcept;

  ring_base(const ring_base&) noexcept = default;
  ring_base(ring_base&&) noexcept = default;

  ring_base& operator=(const ring_base&) noexcept = default;
  ring_base& operator=(ring_base&&) noexcept = default;

  size_type front_index_;
  size_type back_index_;
  size_type write_index_;

 private:
  inline auto down_cast() noexcept -> Detail*;
  inline auto down_cast() const noexcept -> const Detail*;

  size_type drop_count_;
  bool drop_flag_;
};

}  // namespace detail

template <class Type, unsigned Size>
class ring_array : public detail::ring_base<Type, ring_array<Type, Size>> {
  static constexpr bool IsPow2{Size != 0 && (Size & (Size - 1)) == 0};
  using base = detail::ring_base<Type, ring_array>;

 public:
  using value_type = typename base::value_type;
  using pointer = value_type*;
  using const_pointer = const value_type*;
  using size_type = typename base::size_type;
  using iterator = detail::ring_iterator_base<ring_array>;
  using reverse_iterator = std::reverse_iterator<iterator>;

  static_assert(Size > 1, "Size must be greater than 1");
  static_assert(((~size_type{0}) >> 2) + 1 >= Size, "Size is too large");

  ring_array(value_type init) noexcept;
  ring_array() noexcept;

  ring_array(const ring_array&) noexcept = default;
  ring_array(ring_array&&) noexcept = default;

  ring_array& operator=(const ring_array&) noexcept = default;
  ring_array& operator=(ring_array&&) noexcept = default;

  inline auto raw_data() noexcept -> pointer;
  inline auto raw_data() const noexcept -> const_pointer;
  inline auto raw_size() const noexcept -> size_type;

  inline auto begin() noexcept -> iterator;
  inline auto end() noexcept -> iterator;

  inline auto rbegin() noexcept -> reverse_iterator;
  inline auto rend() noexcept -> reverse_iterator;

 private:
  friend class detail::ring_base<Type, ring_array>;
  friend class detail::ring_iterator_base<ring_array>;

  inline static auto ring_index(size_type index, size_type) noexcept
      -> size_type;

  using difference_type = typename iterator::difference_type;
  inline static auto ring_iterator(difference_type diff, size_type) noexcept
      -> size_type;

  value_type buffer_[Size];
};

template <class Type>
class ring_vector : public detail::ring_base<Type, ring_vector<Type>> {
  using base = detail::ring_base<Type, ring_vector>;

 public:
  using value_type = typename base::value_type;
  using pointer = value_type*;
  using const_pointer = const value_type*;
  using size_type = typename base::size_type;
  using iterator = detail::ring_iterator_base<ring_vector>;
  using reverse_iterator = std::reverse_iterator<iterator>;

  ring_vector(size_type size, value_type init);
  ring_vector(size_type size);

  ring_vector(const ring_vector&) = default;
  ring_vector(ring_vector&&) = default;

  ring_vector& operator=(const ring_vector&) noexcept = default;
  ring_vector& operator=(ring_vector&&) noexcept = default;

  inline auto raw_data() noexcept -> pointer;
  inline auto raw_data() const noexcept -> const_pointer;
  inline auto raw_size() const noexcept -> size_type;

  inline auto begin() noexcept -> iterator;
  inline auto end() noexcept -> iterator;

  inline auto rbegin() noexcept -> reverse_iterator;
  inline auto rend() noexcept -> reverse_iterator;

 private:
  friend class detail::ring_base<Type, ring_vector>;
  friend class detail::ring_iterator_base<ring_vector>;

  inline static auto ring_index(size_type index, size_type size) noexcept
      -> size_type;

  using difference_type = typename iterator::difference_type;
  inline static auto ring_iterator(difference_type diff,
                                   size_type size) noexcept -> size_type;

  std::vector<value_type> buffer_;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// implementation
//
//

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ring_iterator_base
//

template <class RingContainer>
detail::ring_iterator_base<RingContainer>::ring_iterator_base(
    pointer buffer, size_type size, size_type index) noexcept
    : buffer_{buffer},
      size_{size},
      index_{index},
      diff_{static_cast<difference_type>(index_)} {}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator==(
    const self& rhs) const noexcept -> bool {
  return &buffer_[index_] == &rhs.buffer_[rhs.index_];
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator!=(
    const self& rhs) const noexcept -> bool {
  return !(*this == rhs);
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator<(
    const self& rhs) const noexcept -> bool {
  return diff_ < rhs.diff_;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator<=(
    const self& rhs) const noexcept -> bool {
  return diff_ <= rhs.diff_;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator>(
    const self& rhs) const noexcept -> bool {
  return diff_ > rhs.diff_;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator>=(
    const self& rhs) const noexcept -> bool {
  return diff_ >= rhs.diff_;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator+=(
    difference_type rhs) noexcept -> self& {
  diff_ += rhs;
  index_ = RingContainer::ring_iterator(diff_, size_);
  return *this;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator+(
    difference_type rhs) noexcept -> self {
  self tmp{*this};
  tmp += rhs;
  return tmp;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator-=(
    difference_type rhs) noexcept -> self& {
  diff_ -= rhs;
  index_ = RingContainer::ring_iterator(diff_, size_);
  return *this;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator-(
    difference_type rhs) noexcept -> self {
  self tmp{*this};
  tmp -= rhs;
  return tmp;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator-(
    const self& rhs) noexcept -> difference_type {
  return diff_ - rhs.diff_;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator++() noexcept
    -> self& {
  ++diff_;
  index_ = RingContainer::ring_iterator(diff_, size_);
  return *this;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator++(int) noexcept
    -> self {
  self tmp{*this};
  ++*this;
  return tmp;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator--() noexcept
    -> self& {
  --diff_;
  index_ = RingContainer::ring_iterator(diff_, size_);
  return *this;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator--(int) noexcept
    -> self {
  self tmp{*this};
  --*this;
  return tmp;
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator->() noexcept
    -> reference {
  return buffer_[index_];
}

template <class RingContainer>
inline auto
detail::ring_iterator_base<RingContainer>::operator->() const noexcept
    -> const_reference {
  return buffer_[index_];
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator*() noexcept
    -> reference {
  return buffer_[index_];
}

template <class RingContainer>
inline auto detail::ring_iterator_base<RingContainer>::operator*()
    const noexcept -> const_reference {
  return buffer_[index_];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ring_base
//

template <class Type, class Detail>
detail::ring_base<Type, Detail>::ring_base() noexcept {
  clear();
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::down_cast() noexcept -> Detail* {
  return static_cast<Detail*>(this);
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::down_cast() const noexcept
    -> const Detail* {
  return static_cast<const Detail*>(this);
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::size() const noexcept
    -> size_type {
  if (write_index_ >= front_index_) {
    return write_index_ - front_index_;

  } else {
    return down_cast()->raw_size() - front_index_ + write_index_;
  }
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::max_size() const noexcept
    -> size_type {
  return down_cast()->raw_size() - 1;
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::empty() const noexcept -> bool {
  return front_index_ == write_index_;
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::full() const noexcept -> bool {
  return size() == max_size();
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::front() noexcept -> reference {
  return down_cast()->buffer_[front_index_];
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::front() const noexcept
    -> const_reference {
  return down_cast()->buffer_[front_index_];
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::back() noexcept -> reference {
  return down_cast()->buffer_[back_index_];
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::back() const noexcept
    -> const_reference {
  return down_cast()->buffer_[back_index_];
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::drop_count() const noexcept
    -> size_type {
  return drop_count_;
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::drop_value() noexcept
    -> reference {
  if (drop_flag_) {
    --drop_count_;
    drop_flag_ = false;
  }
  return down_cast()->buffer_[write_index_];
}

template <class Type, class Detail>
inline auto detail::ring_base<Type, Detail>::drop_value() const noexcept
    -> const_reference {
  if (drop_flag_) {
    --drop_count_;
    drop_flag_ = false;
  }
  return down_cast()->buffer_[write_index_];
}

template <class Type, class Detail>
inline void detail::ring_base<Type, Detail>::inc() noexcept {
  back_index_ = write_index_;
  write_index_ = Detail::ring_index(write_index_ + 1, down_cast()->raw_size());
  if (empty()) {
    ++drop_count_;
    drop_flag_ = true;
    front_index_ =
        Detail::ring_index(front_index_ + 1, down_cast()->raw_size());
  }
}

template <class Type, class Detail>
inline void detail::ring_base<Type, Detail>::push(const_reference x) noexcept {
  inc();
  down_cast()->back() = x;
}

template <class Type, class Detail>
inline void detail::ring_base<Type, Detail>::push_back(
    const_reference x) noexcept {
  push(x);
}

template <class Type, class Detail>
inline void detail::ring_base<Type, Detail>::pop() noexcept {
  if (not empty())
    front_index_ =
        Detail::ring_index(front_index_ + 1, down_cast()->raw_size());
}

template <class Type, class Detail>
inline void detail::ring_base<Type, Detail>::pop_n(size_type n) noexcept {
  if (n < size()) {
    front_index_ =
        Detail::ring_index(front_index_ + n, down_cast()->raw_size());

  } else {
    front_index_ = write_index_;
  }
}

template <class Type, class Detail>
inline void detail::ring_base<Type, Detail>::clear() noexcept {
  front_index_ = 0;
  back_index_ = max_size();
  write_index_ = 0;
  drop_count_ = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ring_array
//

template <class Type, unsigned Size>
ring_array<Type, Size>::ring_array(value_type init) noexcept
    : base(), buffer_{init} {}

template <class Type, unsigned Size>
ring_array<Type, Size>::ring_array() noexcept : base() {}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::raw_data() noexcept -> pointer {
  return buffer_;
}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::raw_data() const noexcept -> const_pointer {
  return buffer_;
}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::raw_size() const noexcept -> size_type {
  return Size;
}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::begin() noexcept -> iterator {
  return {buffer_, Size, this->front_index_};
}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::end() noexcept -> iterator {
  return {buffer_, Size, this->write_index_};
}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::rbegin() noexcept -> reverse_iterator {
  return reverse_iterator(end());
}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::rend() noexcept -> reverse_iterator {
  return reverse_iterator(begin());
}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::ring_index(size_type index,
                                               size_type) noexcept
    -> size_type {
  if constexpr (IsPow2) {
    return index & (Size - 1);

  } else {
    return index % Size;
  }
}

template <class Type, unsigned Size>
inline auto ring_array<Type, Size>::ring_iterator(difference_type diff,
                                                  size_type) noexcept
    -> size_type {
  if constexpr (IsPow2) {
    return static_cast<size_type>(diff) & (Size - 1);

  } else {
    const auto mod{std::abs(diff % static_cast<difference_type>(Size))};

    if (diff < 0) {
      return Size - mod;

    } else {
      return mod;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ring_vector
//

template <class Type>
ring_vector<Type>::ring_vector(size_type size, value_type init) : base() {
  assert(size > 1);
  assert(((~size_type{0}) >> 2) + 1 >= size);
  buffer_.resize(size);
  std::fill(buffer_.begin(), buffer_.end(), init);
}

template <class Type>
ring_vector<Type>::ring_vector(size_type size) : base() {
  assert(size > 1);
  assert(((~size_type{0}) >> 2) + 1 >= size);
  buffer_.resize(size);
}

template <class Type>
inline auto ring_vector<Type>::raw_data() noexcept -> pointer {
  return buffer_;
}

template <class Type>
inline auto ring_vector<Type>::raw_data() const noexcept -> const_pointer {
  return buffer_;
}

template <class Type>
inline auto ring_vector<Type>::raw_size() const noexcept -> size_type {
  return buffer_.size();
}

template <class Type>
inline auto ring_vector<Type>::begin() noexcept -> iterator {
  return {buffer_.data(), static_cast<size_type>(buffer_.size()),
          this->front_index_};
}

template <class Type>
inline auto ring_vector<Type>::end() noexcept -> iterator {
  return {buffer_.data(), static_cast<size_type>(buffer_.size()),
          this->write_index_};
}

template <class Type>
inline auto ring_vector<Type>::rbegin() noexcept -> reverse_iterator {
  return reverse_iterator(end());
}

template <class Type>
inline auto ring_vector<Type>::rend() noexcept -> reverse_iterator {
  return reverse_iterator(begin());
}

template <class Type>
inline auto ring_vector<Type>::ring_index(size_type index,
                                          size_type size) noexcept
    -> size_type {
  return index % size;
}

template <class Type>
inline auto ring_vector<Type>::ring_iterator(difference_type diff,
                                             size_type size) noexcept
    -> size_type {
  const auto mod{std::abs(diff % static_cast<difference_type>(size))};
  if (diff < 0) {
    return size - mod;

  } else {
    return mod;
  }
}

}  // namespace buff